import { Application } from 'express'
import cors from 'cors'
import parser from 'body-parser'
import compression from 'compression'

const handleCors = (app: Application): void => {
  app.use(cors({ credentials: true, origin: true }))
}

const handleCompression = (app: Application): void => {
  app.use(compression())
}

const handleParser = (app: Application): void => {
  app.use(parser.json())
  app.use(parser.urlencoded({ extended: true }))
}

export default [handleCompression, handleCors, handleParser]
