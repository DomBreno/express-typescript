import 'dotenv/config'

export default {
  server: {
    port: process.env.NODE_PORT || 3333
  }
}
