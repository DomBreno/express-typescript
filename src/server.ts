import 'module-alias/register'
import http from 'http'
import express from 'express'

import config from '@config'

import middlewares from '@middlewares/index'
import { applyMiddlewares, applyRoutes } from '@utils/index'

import routes from './services'

const app = express()

applyMiddlewares(middlewares, app)
applyRoutes(routes, app)

const server = http.createServer(app)

server.listen(config.server.port, () =>
  console.info(` \n :: Server running at: http://localhost:${config.server.port} :: \n `)
)
