import { Application, NextFunction, Request, Response } from 'express'

type Wrapper = (app: Application) => void

export const applyMiddlewares = (middlewares: Wrapper[], app: Application): void => {
  middlewares.map(middleware => middleware(app))
}

type Handler = (req: Request, res: Response, next: NextFunction) => Promise<void> | void

type Route = {
  path: string
  method: string
  handler: Handler | Handler[]
}

export const applyRoutes = (routes: Route[], app: Application): void => {
  routes.map(route => {
    const { method, path, handler } = route
    return app[method](path, handler)
  })
}
