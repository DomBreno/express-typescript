import { Hello } from './hello.controller'

export default [
  {
    path: '/hello',
    method: 'get',
    handler: Hello
  }
]
