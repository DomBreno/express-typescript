import { Request, Response } from 'express'

export const Hello = async (req: Request, res: Response): Promise<void> => {
  res.send('Hello from Node.js')
}
